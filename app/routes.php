<?php
use Framework\Routing\Router;
use Framework\Routing\Route;

Router::addRoute(new \Framework\Routing\Route('user/{user_name}/group/{group_name}', 'HelloController@data', Route::METHOD_GET));